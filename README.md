# README #


Aplicação com implementação de regra descrita na Pergunta 03.


### Como testar? ###


Este projeto não tem nenhuma dependência fora do core do Java.


Para executar o analisador deve-se iniciar a classe principal com.femelo.exam.textAnalyzer.TextAnalyzerBoot passando em seguida o argumento com o texto a ser analisado.

Ex:

```
$ java -classpath bin/ com/femelo/exam/textAnalyzer/TextAnalyzerBoot bbbabilllllibe
```

Caso seja encontrado algum caractere válido, será exibida uma mensagem como a seguinte

```
FIRST CHAR: e
Texto analizado:
bbbabilllllibe
```

Caso não seja encontrado algum caractere válido, sera exibida uma mensagem assim:

```
Não foi possível encontrar uma vogal que seja antecedido por uma consoante que seja antecedida por uma vogal.
Texto analizado:
bbbabilllllib
```

### Testes Unitários ###

Desenvolvi alguns testes unitários sem framework de testes, somente para auxiliar na validação das regras. Para rodar os testes deve-se iniciar a classe principal com.femelo.exam.textAnalyzer.core.TextAnalyzerTest.


Ex:

```
$ java -classpath bin/ com/femelo/exam/textAnalyzer/core/TextAnalyzerTest
```

O resultado será como o seguinte:

```
SUCCESS should return * for empty string: expected <*>, actual <*>.
SUCCESS should return * for 1 vowel string: expected <*>, actual <*>.
SUCCESS should return * for 2 vowel string: expected <*>, actual <*>.
SUCCESS should return a for 3 vowel successful string: expected <a>, actual <a>.
SUCCESS should return * for only consonant string: expected <*>, actual <*>.
SUCCESS should return e for string with successful vowel in the middle: expected <e>, actual <e>.
SUCCESS should return * for string with vowel invalidated by late repetition: expected <*>, actual <*>.
SUCCESS should return i for string with 2 successful vowels: expected <i>, actual <i>.
SUCCESS should return o for string with vowel invalidated by late repetition if other successful vowel is found: expected <o>, actual <o>.
SUCCESS should return * for string successful with a lowercase vowel and considered duplicated with a uppercase equal: expected <*>, actual <*>.
SUCCESS should return U for string with successful uppercase vowel: expected <U>, actual <U>.

#######
SUCCESS
#######
```