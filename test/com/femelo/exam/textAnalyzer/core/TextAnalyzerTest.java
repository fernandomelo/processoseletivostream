package com.femelo.exam.textAnalyzer.core;

public class TextAnalyzerTest {
	private static boolean failed = false;
	
	public static void main(String args[]) {
		
		assertEquals("should return * for empty string", '*', TextAnalyzer.firstChar("".chars()));
		assertEquals("should return * for 1 vowel string", '*', TextAnalyzer.firstChar("a".chars()));
		assertEquals("should return * for 2 vowel string", '*', TextAnalyzer.firstChar("ae".chars()));
		assertEquals("should return a for 3 vowel successful string", 'a', TextAnalyzer.firstChar("iba".chars()));
		assertEquals("should return * for only consonant string", '*', TextAnalyzer.firstChar("ktghtrkscdrflm".chars()));
		assertEquals("should return e for string with successful vowel in the middle", 'e', TextAnalyzer.firstChar("ictwatetjkd".chars()));
		assertEquals("should return * for string with vowel invalidated by late repetition", '*', TextAnalyzer.firstChar("ictwatutjkdu".chars()));
		assertEquals("should return i for string with 2 successful vowels", 'i', TextAnalyzer.firstChar("uctwatitjudo".chars()));
		assertEquals("should return o for string with vowel invalidated by late repetition if other successful vowel is found", 'o', TextAnalyzer.firstChar("uctwatitjudoi".chars()));
		assertEquals("should return * for string successful with a lowercase vowel and considered duplicated with a uppercase equal", '*', TextAnalyzer.firstChar("uctwatitjudoiO".chars()));
		assertEquals("should return U for string with successful uppercase vowel", 'U', TextAnalyzer.firstChar("octwatitjodUi".chars()));
		
		if (failed) {
			System.err.println("");
			System.err.println("######");
			System.err.println("FAILED");
			System.err.println("######");
			System.err.println("");
		}
		else{
			System.out.println("");
			System.out.println("#######");
			System.out.println("SUCCESS");
			System.out.println("#######");
			System.out.println("");
		}
	}
	
	private static void assertEquals(String name, char expected, char actual) {
		if (expected == actual)
			System.out.println("SUCCESS " + name + ": expected <" + expected + ">, actual <" + actual + ">.");
		else {
			failed = true;
			System.err.println("FAILED " + name + ": expected <" + expected + ">, actual <" + actual + ">.");
		}
	}
}
