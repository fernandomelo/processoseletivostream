package com.femelo.exam.textAnalyzer.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;

public class TextAnalyzer {
	//listas para fácil verificação do tipo do caractere (vogal ou consoante)
	private static final List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u');
	private static final List<Character> consonant = Arrays.asList('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z');

	//lista para gravar vogais que já passaram pela checagem e não atenderam às condições
	private static List<Character> ignore = new ArrayList<Character>();
	
	//lista para gravar as vogais que atendem às condições
	private static List<Character> matches = new ArrayList<Character>();
	
	public static char firstChar(BaseStream input) {
		initialize();
		
		Iterator iterator = input.iterator();
		
		//armazenamento dos caracteres relevantes
		char penult = '*';
		char previous = '*';
		char current = '*';
		
		//executar até o fim do Stream 
		while (iterator.hasNext()) {
			
			//guarda o valor original do char 
			char original = getNextAsChar(iterator);
			
			//converte pra minúsculo num auxiliar para facilitar comparações
			char aux = Character.toLowerCase(original);
			
			//atualiza as variaveis de comparação
			penult = previous;
			previous = current;
			current = aux;
			
			//para retornar o resultado, eu guardo a variável na forma original. Se o usuário digitar letras maiúsculas,
			//o app vai apontar o resultado mostrando uma letra maiúscula. Mas para comparações eu preciso deste temporário
			//totalmente minúsculo
			List<Character> tempNormalizedMatches = matches.stream().map(x -> Character.toLowerCase(x)).collect(Collectors.toList());
			
			if (validateConditions(penult, previous, current, tempNormalizedMatches))
				
				//atendeu a todas as condições necessesárias, então adiciona na lista de resultados
				matches.add(original);
			
			else if (tempNormalizedMatches.contains(current)) {
				
				//não atendeu a todas as condições e era considerado um resultado, mas acabamos de encontrar uma duplicata
				//entao deve ser removido dos resultados
				removeRepeatedMatch(current, tempNormalizedMatches);
				
				//e deve passar a ser ignorada
				ignore.add(current);
			}
			else if (isVowel(current)) {
				
				//se nao atende as outras condições, mas é uma vogal, passamos a ignora-la,
				//pois por mais que haja mais a frente a mesma vogal atendendo às demais condições, a condição de unicidade não 
				//poderá mais ser atendida para essa vogal
				ignore.add(current);
			}
		}
		
		return getBestMatchOrDefault('*');
	}

	private static char getBestMatchOrDefault(char defaultChar) {
		//pega o primeiro caractere não invalidado da lista de matches
		if (matches.stream().filter(x -> x != '*').toArray().length > 0)
			return (char) matches.stream().filter(x -> x != '*').toArray()[0];
		
		//ou retorna o caractere default
		return defaultChar;
	}

	private static void removeRepeatedMatch(char current, List<Character> tempNormalizedMatches) {
		int indexOf = tempNormalizedMatches.indexOf(current);
		matches.set(indexOf, '*');
	}

	private static boolean validateConditions(char penult, char previous, char current, List<Character> tempNormalizedMatches) {
		return isVowel(penult) //a penultima letra deve ser vogal
				&& isConsonant(previous) //a ultima deve ser consoante
				&& isVowel(current) //a atual deve ser uma vogal
				&& !ignore.contains(current)  //não pode ser uma vogal que já passou pela validação
				&& !tempNormalizedMatches.contains(current); //ela ainda não pode estar na lista de resultados
	}

	private static char getNextAsChar(Iterator iterator) {
		return (char) ((Integer) iterator.next()).intValue();
	}
	
	private static boolean isVowel(char c) {
		return vowels.contains(c);
	}
	
	private static boolean isConsonant(char c) {
		return consonant.contains(c);
	}
	
	private static void initialize() {
		ignore = new ArrayList<Character>();
		matches = new ArrayList<Character>();
	}
}
