package com.femelo.exam.textAnalyzer;

import com.femelo.exam.textAnalyzer.core.TextAnalyzer;

public class TextAnalyzerBoot {

	public static void main (String args[]) {
		if (args.length == 0) {
			System.out.println("Informe uma sequência de caracteres para ser analisada. EX: <TextAnalyzer> texto");
			return;
		}
		
		CharSequence s = args[0];
		
		char lastChar = TextAnalyzer.firstChar(s.chars());
		
		if (lastChar == '*')
			System.out.println("Não foi possível encontrar uma vogal que seja antecedido por uma consoante que seja antecedida por uma vogal.");
		else
			System.out.println("FIRST CHAR: " + lastChar);
		
		System.out.println("Texto analizado:");
		System.out.println(s);
	}
	
}
